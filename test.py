# -*- coding: utf-8 -*
import os  

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text

def test_SidebarText():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1

# 2. [Screenshot] Side Bar Text

def Sc_SidebarText():
	os.system('adb shell screencap -p /sdcard/screen1.png && adb pull /sdcard/screen1.png ')

# 3. [Context] Categories roll down and tab

def test_Categoriesroll():
	os.system('adb shell input keyevent "KEYCODE_BACK"')
	os.system('adb shell input roll 0 1000 && adb shell input tap 1000 300')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美裝') != -1
	assert xmlString.find('衣鞋包錶') != -1

# 4. [Screenshot] Categories

def Sc_Categories():
	os.system('adb shell screencap -p /sdcard/screen2.png && adb pull /sdcard/screen2.png ')

# 5. [Context] Categories page

def test_Categoriespage():
	os.system('adb shell input keyevent "KEYCODE_BACK" && adb shell input tap 400 1700')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美裝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	assert xmlString.find('書店') != -1
	assert xmlString.find('電子票券') != -1
# 6. [Screenshot] Categories page

def Sc_Categories():
	os.system('adb shell screencap -p /sdcard/screen3.png && adb pull /sdcard/screen3.png')

# 7. [Behavior] Search item “switch”
def Search_item():
	os.system('adb shell input tap 800 100')
	os.system('adb shell input text "switch" && adb shell input keyevent "KEYCODE_ENTER"')

# 8. [Behavior] Follow an item and it should be add to the list
def Follow_item():
	os.system('adb shell input tap 1000 1000 && adb shell input tap 100 1700')
	os.system('adb shell input keyevent "KEYCODE_BACK" && adb shell input keyevent "KEYCODE_BACK" ')
	os.system('adb shell input tap 100 100 && adb shell input tap 100 800') 

# 9. [Behavior] Navigate tto the detail of item 
# Roll down and tap 800 100
def Detail_item():
	os.system('adb shell input tap 200 1000 && adb shell input roll 0 500')
	os.system('adb shell input tap 600 200')

# 10. [Screenshot] Disconnetion Screen

def Sc_Disconnection():
	os.system('adb shell svc data disable')
	os.system('adb shell screencap -p /sdcard/screen4.png && adb pull /sdcard/screen4.png ')
